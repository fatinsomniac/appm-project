@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @auth
                    {{ __('Dashboard') }}
                        @else
                    <h1>Hi, Selamat Datang Di Aplikasi Pengolahan Pengaduan Masyarakat</h1> 
                    @endauth
                </div>

                <div class="card-body">
                    @auth
                        @if (auth()->user()->id == 1)
                            Hi, <font color="red">Admin</font> <b>{{ Auth::user()->name }}</b> Selamat Datang di Aplikasi Pengolahan Pengaduan Masyarakat
                        @elseif (auth()->user()->id == 2)
                            Hi, <font color="blue">Petugas</font> <b>{{ Auth::user()->name }}</b> Selamat Datang di Aplikasi Pengolahan Pengaduan Masyarakat
                        @else 
                            Hi, <b>{{ Auth::user()->name }}</b> Selamat Datang di Aplikasi Pengolahan Pengaduan Masyarakat 
                            <br>
                            Silahkan adukan jika ada keluhan dari dirimu terhadap Pemerintah
                        @endif
                    @else
                    <b>
                    Silahkan adukan jika ada keluhan dari dirimu terhadap Pemerintah, <br>
                    Silahkan login terlebih dahulu jika kamu ingin mengadukan sesuatu.
                    </b>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
