<div class="form-group">
    <label for="title">Upload Bukti Pengaduan</label> <br>
    <input type="file" name="thumbnail" id="thumbnail">
    @error('thumbnail')
        <div class="text-danger mt-2">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <label for="title">Judul Pengaduan</label>
    <input type="text" name="title" id="title" value="{{ old('title') ?? $post->title }}" class="form-control">
    @error('title')
        <div class="text-danger mt-2">
            {{ $message }}
        </div>
    @enderror
</div>

<div class="form-group">
    <label for="category">Kategori Pengaduan</label>
    <select name="category" id="category" class="form-control">
        <option disabled selected>Choose one!</option>
        @foreach ($categories as $category)
            <option {{ $category->id == $post->category_id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>
    @error('category')
        <div class="text-danger mt-2">
            {{ $message }}
        </div>
    @enderror
</div>

<div class="form-group">
    <label for="tags">Tag Pengaduan</label>
    <select name="tags[]" id="tags" class="form-control select2" multiple>
        @foreach ($post->tags as $tag)
            <option selected value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach

        @foreach ($tags as $tag)
            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
    @error('tags')
        <div class="text-danger mt-2">
            {{ $message }}
        </div>
    @enderror
</div>

<div class="form-group">
    <label for="comment">Komentar</label>
    <textarea name="comment" id="comment" class="form-control">{{ old('comment') ?? $post->comment }}</textarea>
    @error('comment')
        <div class="text-danger mt-2">
            {{ $message }}
        </div>
    @enderror
</div>

<div class="form-group">
    <label for="body">Keterangan</label>
    <textarea name="body" id="body" class="form-control">{{ old('body') ?? $post->body }}</textarea>
    @error('body')
        <div class="text-danger mt-2">
            {{ $message }}
        </div>
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Perbaharui Pengaduan' }}</button>