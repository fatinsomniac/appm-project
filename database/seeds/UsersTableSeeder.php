<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Monkey D. Luffy',
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'email' => 'luffy@gmail.com',
        ]);

        \App\User::create([
            'name' => 'Roronoa Zoro',
            'username' => 'petugas',
            'password' => bcrypt('petugas'),
            'email' => 'zoro@gmail.com',
        ]);
    }
}
